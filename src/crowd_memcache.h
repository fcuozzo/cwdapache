/**
* Header file for the Memcached support on the Atlassian Crowd Connector.
*
*/

#include <stdbool.h>

/* Apache httpd includes */
#include "httpd.h"
#include "apr.h"
#include "apr_memcache.h"

typedef struct {
    const char *host;
    apr_uint64_t port;
    apr_uint64_t entry_timeout;
    apr_uint64_t failed_attempts_window;	/* Range when the max_failed_attempts must occur to blacklist the user */
    apr_uint64_t max_failed_attempts;    	/* How many times the user can try to login with a wrong password */
    apr_uint64_t blacklist_timeout;     	/* How long we blacklist the user for, so he won't be able to login
					   unless he changes his password or the cache entry expires and the
					   password starts to be valid */

    // Memcached connection pool settings
    apr_uint64_t pool_min; 				/* minimum number of client sockets to open */
    apr_uint64_t pool_smax; 				/* soft maximum number of client connections to open */
    apr_uint64_t pool_max; 				/* hard maximum number of client connections */
    apr_uint64_t pool_ttl; 				/* time to live in seconds of a client connection */ 
    
    apr_memcache_t *memcache;
    apr_memcache_server_t *server;

} memcached_config_t;

/**
 * Creates a memcached_config_t, populated with default values.
 * 
 * @param p	The APR pool from which to allocate memory.
 * @returns	A pointer to the memcached_config_t, or NULL upon failure.
 */
memcached_config_t *crowd_create_memcached_config(apr_pool_t *p);

apr_status_t crowd_setup_memcached(const server_rec *s, apr_pool_t *p, memcached_config_t *memcached_config);

apr_status_t crowd_test_memcached_connection(const server_rec *s, apr_pool_t *p, memcached_config_t *memcached_config);
