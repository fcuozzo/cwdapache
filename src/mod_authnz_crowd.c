/* Standard headers */
#include <stdbool.h>
#include <string.h>

/* Apache Portable Runtime includes */
#include "apr_strings.h"
#include "apr_xlate.h"

/* Apache httpd includes */
#include "ap_provider.h"
#include "httpd.h"
#include "http_core.h"
#include "http_config.h"
#include "http_log.h"
#include "http_request.h"
#include "mod_auth.h"

#undef PACKAGE_BUGREPORT
#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION

#include "../config.h"

#include "crowd_client.h"
#include "util.h"

#include "mod_authnz_crowd.h"

static struct
{
    const char *cache_max_entries_string;
    const char *cache_max_age_string;
} authnz_crowd_process_config;

typedef struct
{
    bool authoritative;
    bool authoritative_set;
    const char *crowd_timeout_string;
    apr_array_header_t *basic_auth_encodings;
    crowd_config *crowd_config;
    bool accept_sso;
    bool accept_sso_set;
    bool create_sso;
    bool create_sso_set;
    bool ssl_verify_peer_set;

} authnz_crowd_dir_config;

typedef struct
{
    memcached_config_t *memcached_config;
} authnz_crowd_server_config;

module AP_MODULE_DECLARE_DATA authnz_crowd_module;

static apr_array_header_t *dir_configs = NULL;

static authnz_crowd_server_config* crowd_get_server_config(server_rec *s);

/** Function to allow all modules to create per directory configuration
 *  structures.
 *  @param p The pool to use for all allocations.
 *  @param dir The directory currently being processed.
 *  @return The per-directory structure created
 */
static void *create_dir_config(apr_pool_t *p, char *dir)
{
    ap_log_perror(APLOG_MARK, APLOG_DEBUG, 0, p, "Creating Crowd config for '%s'", dir);
    authnz_crowd_dir_config *dir_config = log_palloc(p, apr_pcalloc(p, sizeof(authnz_crowd_dir_config)));
    if (dir_config == NULL) {
        exit(1);
    }
    dir_config->authoritative = true;
    dir_config->accept_sso = true;
    dir_config->create_sso = true;
    dir_config->crowd_config = crowd_create_config(p);
    if (dir_config->crowd_config == NULL) {
        exit(1);
    }

    dir_config->crowd_config->crowd_ssl_verify_peer = true;
    dir_config->basic_auth_encodings = log_palloc(p, apr_array_make(p, 0, sizeof(char *)));
    if (dir_config->basic_auth_encodings == NULL) {
        exit(1);
    }

    // Add new config to list of this module's per-directory configs, for checking during the post-config phase.
    if (dir_configs == NULL)  {
        dir_configs = log_palloc(p, apr_array_make(p, 0, sizeof(authnz_crowd_dir_config *)));
        if (dir_configs == NULL) {
            exit(1);
        }
    }
    APR_ARRAY_PUSH(dir_configs, authnz_crowd_dir_config *) = dir_config;

    return dir_config;
}

static void *create_svr_config(apr_pool_t *p, server_rec *s)
{
    ap_log_perror(APLOG_MARK, APLOG_DEBUG, 0, p, "Creating Crowd server config");

    authnz_crowd_server_config *svr_config = apr_pcalloc(p, sizeof(authnz_crowd_server_config));

    svr_config->memcached_config = crowd_create_memcached_config(p);
    if (svr_config->memcached_config == NULL) {
        exit(1);
    }

    return svr_config;
}

static const char *set_once_error(const cmd_parms *parms)
{
    const char *error
        = log_palloc(parms->temp_pool, apr_psprintf(parms->temp_pool, "%s specified multiple times", parms->cmd->name));
    if (error == NULL) {
        error = "Out of memory";
    }
    return error;
}

static const char *set_once(const cmd_parms *parms, const char **location, const char *w)
{
    if (*location != NULL) {
        return set_once_error(parms);
    }
    *location = log_palloc(parms->temp_pool, apr_pstrdup(parms->pool, w));
    if (*location == NULL) {
        return "Out of memory";
    }
    return NULL;
}

static const char *set_flag_once(const cmd_parms *parms, bool *location, bool *set_location, int on) {
    if (*set_location) {
        return set_once_error(parms);
    }
    *location = on;
    *set_location = true;
    return NULL;
}

static const char *set_authz_crowd_authoritative(cmd_parms *parms, void *mconfig, int on)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_flag_once(parms, &(config->authoritative), &(config->authoritative_set), on);
}

static const char *set_crowd_app_name(cmd_parms *parms, void *mconfig, const char *w)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_config->crowd_app_name), w);
}

static const char *set_crowd_app_password(cmd_parms *parms, void *mconfig, const char *w)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_config->crowd_app_password), w);
}

static const char *add_basic_auth_conversion(const char *encoding, authnz_crowd_dir_config *config, apr_pool_t *ptemp)
{
    apr_xlate_t *conversion;
    if (apr_xlate_open(&conversion, "UTF-8", encoding, ptemp) != APR_SUCCESS) {
        const char *error = log_palloc(ptemp, apr_psprintf(ptemp, "Encoding not supported: '%s'", encoding));
        if (error == NULL) {
            error = "Out of memory";
        }
        return error;
    }
    apr_xlate_close(conversion);
    ap_log_perror(APLOG_MARK, APLOG_DEBUG, 0, ptemp, "Configured to use basic authentication encoding: '%s'", encoding);
    APR_ARRAY_PUSH(config->basic_auth_encodings, const char*) = encoding;
    return NULL;
}

static const char *set_crowd_basic_auth_encoding(cmd_parms *parms, void *mconfig, const char *w)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return add_basic_auth_conversion(w, config, parms->temp_pool);
}

static const char *set_crowd_timeout(cmd_parms *parms, void *mconfig, const char *w)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_timeout_string), w);
}

static const char *set_crowd_cert_path(cmd_parms *parms, void *mconfig, const char *w)
{
    // Ignore empty URLs.  Will be reported as a missing parameter.
    if (*w == '\0') {
        return;
    }

    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_config->crowd_cert_path), w);
}

static const char *set_crowd_url(cmd_parms *parms, void *mconfig, const char *w)
{
    // Ignore empty URLs.  Will be reported as a missing parameter.
    if (*w == '\0') {
        return NULL;
    }
    // Add a trailing slash if one does not already exist.
    if (w[strlen(w) - 1] != '/') {
        w = log_palloc(parms->temp_pool, apr_pstrcat(parms->temp_pool, w, "/", NULL));
        if (w == NULL) {
            return "Out of memory";
        }
    }

    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_config->crowd_url), w);
}

static unsigned int parse_number(const char *string, const char *name, unsigned int min, unsigned int max,
    unsigned int default_value, apr_pool_t *p, char **error_msg) {
    *error_msg = NULL;
    if (string == NULL) {
        return default_value;
    }
    errno = 0;
    apr_int64_t value = apr_atoi64(string);
    if (errno != 0 || value < 0 ) {
	*error_msg = apr_psprintf(p, "Could not parse %s: '%s' - (min: %u, max: %u, default: %u, errorno: %d)", name, string, min, max, default_value, errno);
    } else if ((apr_uint64_t)value > max || (apr_uint64_t)value < min) {
	*error_msg = apr_psprintf(p, "Invalid range for property %s: '%s' - (min: %u, max: %u, default: %u)", name, string, min, max, default_value, errno);
    }
    return (unsigned int)value;
}

static const char *set_number(const cmd_parms *parms, const char *paramName, unsigned int min, unsigned int max,
    unsigned int default_value, apr_uint64_t *location, const char *w)
{
    char **error_msg = apr_pcalloc(parms->temp_pool, sizeof(char *));
    *location=parse_number(w, paramName, min, max, default_value, parms->temp_pool, error_msg);
    return *error_msg;
}

static const char *set_crowd_cache_max_age(cmd_parms *parms, void *mconfig __attribute__((unused)),
    const char *w __attribute__((unused)))
{
    return set_once(parms, &(authnz_crowd_process_config.cache_max_age_string), w);
}

static const char *set_crowd_cache_max_entries(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    return set_once(parms, &(authnz_crowd_process_config.cache_max_entries_string), w);
}

static const char *set_crowd_accept_sso(cmd_parms *parms, void *mconfig, int on)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_flag_once(parms, &(config->accept_sso), &(config->accept_sso_set), on);
}

static const char *set_crowd_create_sso(cmd_parms *parms, void *mconfig, int on)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_flag_once(parms, &(config->create_sso), &(config->create_sso_set), on);
}

static const char *set_crowd_ssl_verify_peer(cmd_parms *parms, void *mconfig, int on)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_flag_once(parms, &(config->crowd_config->crowd_ssl_verify_peer), &(config->ssl_verify_peer_set), on);
}

static const char *set_crowd_groups_env_name(cmd_parms *parms, void *mconfig, const char *w)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_config->groups_env_name), w);
}

static const char *set_crowd_memcached_host(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_once(parms, &(config->memcached_config->host), w);
}

static const char *set_crowd_memcached_port(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_number(parms, "CrowdMemcachedPort", 0, UINT_MAX, 11211, &(config->memcached_config->port), w);
}

static const char *set_crowd_memcached_entry_timeout(cmd_parms *parms, void *mconfig __attribute__((unused)),
    const char *w __attribute__((unused)))
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_number(parms, "CrowdMemcachedEntryTimeout", 10, UINT_MAX, 300, &(config->memcached_config->entry_timeout), w);
}

static const char *set_crowd_memcached_failed_attempts_window(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_number(parms, "CrowdMemcachedFailedAttemptsWindow", 10, UINT_MAX, 120, &(config->memcached_config->failed_attempts_window), w);
}


static const char *set_crowd_memcached_blacklist_timeout(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_number(parms, "CrowdMemcachedBlacklistTimeout", 60, UINT_MAX, 600, &(config->memcached_config->blacklist_timeout), w);
}


static const char *set_crowd_memcached_max_failed_attempts(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_number(parms,"CrowdMemcachedMaxFailedAttempts", 0, UINT_MAX, 10, &(config->memcached_config->max_failed_attempts), w);
}


static const char *set_crowd_memcached_pool_min(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_number(parms, "CrowdMemcachedPoolMin", 0, UINT_MAX, 0, &(config->memcached_config->pool_min), w);
}


static const char *set_crowd_memcached_pool_smax(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_number(parms,"CrowdMemcachedPoolSoftMax", 0, UINT_MAX, 10, &(config->memcached_config->pool_smax), w);
}


static const char *set_crowd_memcached_pool_max(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_number(parms, "CrowdMemcachedPoolMax", 1, UINT_MAX, 30, &(config->memcached_config->pool_max), w);
}

static const char *set_crowd_memcached_pool_ttl(cmd_parms *parms, void *mconfig __attribute__((unused)), const char *w)
{
    authnz_crowd_server_config *config = crowd_get_server_config(parms->server);
    return set_number(parms, "CrowdMemcachedPoolTTL", 1, UINT_MAX, 300, &(config->memcached_config->pool_ttl), w);
}

static const command_rec commands[] =
{
    AP_INIT_FLAG("AuthzCrowdAuthoritative", set_authz_crowd_authoritative, NULL, OR_AUTHCFG,
        "'On' if Crowd should be considered authoritative for denying authorisation; "
        "'Off' if a lower-level provider can override authorisation denial by Crowd (default = On)"),
    AP_INIT_TAKE1("CrowdAppName", set_crowd_app_name, NULL, OR_AUTHCFG,
        "The name of this application, as configured in Crowd"),
    AP_INIT_TAKE1("CrowdAppPassword", set_crowd_app_password, NULL, OR_AUTHCFG,
        "The password of this application, as configured in Crowd"),
    AP_INIT_ITERATE("CrowdBasicAuthEncoding", set_crowd_basic_auth_encoding, NULL, OR_AUTHCFG,
        "The list of character encodings that will be used to interpret Basic authentication credentials "
        "(default is ISO-8859-1 only"),
    AP_INIT_TAKE1("CrowdTimeout", set_crowd_timeout, NULL, OR_AUTHCFG,
        "The maximum length of time, in seconds, to wait for a response from Crowd (default or 0 = no timeout)"),
    AP_INIT_TAKE1("CrowdURL", set_crowd_url, NULL, OR_AUTHCFG, "The base URL of the Crowd server"),
    AP_INIT_TAKE1("CrowdCertPath", set_crowd_cert_path, NULL, OR_AUTHCFG, "The path to the SSL certificate file to supply to curl for Crowd over SSL"),
    AP_INIT_TAKE1("CrowdCacheMaxAge", set_crowd_cache_max_age, NULL, RSRC_CONF,
        "The maximum length of time that successful results from Crowd can be cached, in seconds"
        " (default = 60 seconds)"),
    AP_INIT_TAKE1("CrowdCacheMaxEntries", set_crowd_cache_max_entries, NULL, RSRC_CONF,
        "The maximum number of successful results from Crowd that can be cached at any time"
        " (default = 500, 0 = disable cache)"),
    AP_INIT_FLAG("CrowdSSLVerifyPeer", set_crowd_ssl_verify_peer, NULL, OR_AUTHCFG,
            "'On' if SSL certificate validation should occur when connecting to Crowd; 'Off' otherwise (default = On)"),
    AP_INIT_TAKE1("CrowdGroupsEnvName", set_crowd_groups_env_name, NULL, OR_AUTHCFG,
        "Name of the environment variable in which to store a space-delimited list of groups that the remote user belongs to"),
    AP_INIT_TAKE1("CrowdMemcachedEntryTimeout", set_crowd_memcached_entry_timeout, NULL, RSRC_CONF,
        "The maximum length of time that successful results from Crowd can be cached, in seconds"
        " (minimum = 10 seconds, default = 300 seconds)"),
    AP_INIT_TAKE1("CrowdMemcachedHost", set_crowd_memcached_host, NULL, RSRC_CONF,
        "The Memcached host to connect to."
        ),
    AP_INIT_TAKE1("CrowdMemcachedPort", set_crowd_memcached_port, NULL, RSRC_CONF,
        "The Memcached port to connect to."
        " (default = 11211)"),
    AP_INIT_TAKE1("CrowdMemcachedFailedAttemptsWindow", set_crowd_memcached_failed_attempts_window, NULL, RSRC_CONF,
        "The window, in seconds, where a number of failed authentication attempts must be made before blacklisting the user."
        " (default = 120 seconds)"),
    AP_INIT_TAKE1("CrowdMemcachedBlacklistTimeout", set_crowd_memcached_blacklist_timeout, NULL, RSRC_CONF,
        "The length in time that the user will be blacklisted and denied authentication, in seconds"
        " (default = 600 seconds)"),
    AP_INIT_TAKE1("CrowdMemcachedMaxFailedAttempts", set_crowd_memcached_max_failed_attempts, NULL, RSRC_CONF,
        "The number of failed attempts inside the failed attempts window that will make the user be blacklisted"
        " (default = 10)"),
    AP_INIT_TAKE1("CrowdMemcachedPoolMin", set_crowd_memcached_pool_min, NULL, RSRC_CONF,
        "The minimum number of client sockets to open to memcached"
        " (default = 0)"),
    AP_INIT_TAKE1("CrowdMemcachedPoolSoftMax", set_crowd_memcached_pool_smax, NULL, RSRC_CONF,
        "The soft maximum number of client connections to open to memcached"
        " (default = 10)"),
    AP_INIT_TAKE1("CrowdMemcachedPoolMax", set_crowd_memcached_pool_max, NULL, RSRC_CONF,
        "The hard maximum number of client connections to open to memcached"
        " (default = 30)"),
    AP_INIT_TAKE1("CrowdMemcachedPoolTTL", set_crowd_memcached_pool_ttl, NULL, RSRC_CONF,
        "The time to live in seconds of a client connection to memcached"
        " (default = 300)"),
    AP_INIT_FLAG("CrowdAcceptSSO", set_crowd_accept_sso, NULL, OR_AUTHCFG,
        "'On' if single-sign on cookies should be accepted; 'Off' otherwise (default = On)"),
    AP_INIT_FLAG("CrowdCreateSSO", set_crowd_create_sso, NULL, OR_AUTHCFG,
        "'On' if single-sign on cookies should be created; 'Off' otherwise (default = On)"),
    { NULL }
};

static authnz_crowd_dir_config *get_config(request_rec *r) {
    authnz_crowd_dir_config *config
        = (authnz_crowd_dir_config *) ap_get_module_config(r->per_dir_config, &authnz_crowd_module);
    if (config == NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r, "Configuration not found.");
    }
    return config;
}

static authnz_crowd_server_config* crowd_get_server_config(server_rec *s) {
    authnz_crowd_server_config *config
        = (authnz_crowd_server_config *) ap_get_module_config(s->module_config, &authnz_crowd_module);
    if (config == NULL) {
        ap_log_error(APLOG_MARK, APLOG_CRIT, 0, s, "Server configuration not found.");
    }
    return config;
}

typedef struct {
    request_rec *r;
    authnz_crowd_dir_config *config;
    char *cookie_name;
    size_t cookie_name_len;
    char *token;
} check_for_cookie_data_t;

static bool is_https(request_rec *r) {
     const char *https = apr_table_get(r->subprocess_env, "HTTPS");
     return https != NULL && strcmp(https, "on");
}

static int check_for_cookie(void *rec, const char *key, const char *value) {
    if (strcasecmp("Cookie", key) == 0) {
        check_for_cookie_data_t *data = rec;
        if (data->cookie_name == NULL) {
	    authnz_crowd_server_config *server_config = crowd_get_server_config(data->r->server);
            crowd_cookie_config_t *cookie_config = crowd_get_cookie_config(data->r, data->config->crowd_config, server_config->memcached_config);
            if (cookie_config == NULL || cookie_config->cookie_name == NULL || (cookie_config->secure && !is_https(data->r))) {
		if (cookie_config != NULL && cookie_config->cookie_name != NULL) {
    		    ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, data->r, "Tried to authenticate with cookie_secure=1 but it's not HTTP/S, this is suspicious...");
		}
                return 0;
            }
            data->cookie_name = log_ralloc(data->r, apr_pstrcat(data->r->pool, cookie_config->cookie_name, "=", NULL));
 
            if (data->cookie_name == NULL) {
                return 0;
            }
            data->cookie_name_len = strlen(data->cookie_name);
        }
        char *cookies = log_ralloc(data->r, apr_pstrdup(data->r->pool, value));
        if (cookies == NULL) {
            return 0;
        }
        apr_collapse_spaces(cookies, cookies);
        char *last;
        char *cookie = apr_strtok(cookies, ";,", &last);
        while (cookie != NULL) {
            if (strncasecmp(cookie, data->cookie_name, data->cookie_name_len) == 0) {
                data->token = log_ralloc(data->r, apr_pstrdup(data->r->pool, cookie + data->cookie_name_len));
                return 0;
            }
            cookie = apr_strtok(NULL, ";,", &last);
        }
    }
    return 1;
}

#define GRP_ENV_MAX_GROUPS 128
#define GRP_ENV_DELIMITER " "
#define GRP_ENV_DELIMITER_LEN (sizeof(GRP_ENV_DELIMITER) - sizeof(char))
static void crowd_set_groups_env_variable(request_rec *r) {
    authnz_crowd_dir_config *config = get_config(r);
    const char *group_env_name = config->crowd_config->groups_env_name;
    if (group_env_name == NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "CrowdGroupsEnvName undefined; returning.");
        return;
    }

    apr_array_header_t *user_groups = authnz_crowd_user_groups(r->user, r);
    if (user_groups == NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_NOTICE, 0, r, "While setting groups environment variable '%s' for remote user '%s': authnz_crowd_user_groups() returned NULL.", group_env_name, r->user);
        return;
    }

    apr_size_t ngrps = user_groups->nelts;
    if (ngrps <= 0) {
        apr_table_set(r->subprocess_env, group_env_name, "");
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Set groups environment variable '%s' for remote user '%s' to empty.", group_env_name, r->user);
        return;
    }

    if (ngrps > GRP_ENV_MAX_GROUPS) {
        ap_log_rerror(APLOG_MARK, APLOG_NOTICE, 0, r, "While setting groups environment variable '%s' for remote user '%s': Value will be clipped as number of groups (%d) exceeds GRP_ENV_MAX_GROUPS (%d).", group_env_name, r->user, ngrps, GRP_ENV_MAX_GROUPS);
        ngrps = GRP_ENV_MAX_GROUPS;
    }

    apr_size_t nvec = ngrps + (ngrps - 1); /* Groups + conjunctive delimiters */
    struct iovec *iov = apr_pcalloc(r->pool, sizeof(struct iovec) * nvec);
    int i, k;
    for (i = 0, k = 0; i < ngrps; i++) {
        if (i >= 1) {
            /* Join previous entry with a delimiter */
            iov[k].iov_base = GRP_ENV_DELIMITER;
            iov[k].iov_len = GRP_ENV_DELIMITER_LEN;
            k++;
        }

        void *grp = APR_ARRAY_IDX(user_groups, i, void *);
        iov[k].iov_base = grp;
        iov[k].iov_len = strlen(grp);
        k++;
    }

    const char *groups = apr_pstrcatv(r->pool, iov, nvec, NULL);
    if (groups == NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_NOTICE, 0, r, "While setting groups environment variable '%s' for remote user '%s': apr_pstrcatv() returned NULL.", group_env_name, r->user);
        return;
    }

    apr_table_set(r->subprocess_env, group_env_name, groups);
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Set groups environment variable '%s' for remote user '%s' to '%s'", group_env_name, r->user, groups);
}

static int check_user_id(request_rec *r) {
    authnz_crowd_dir_config *config = get_config(r);
    if (config == NULL || !(config->accept_sso)) {
        return DECLINED;
    }
    check_for_cookie_data_t data = { .r = r, .config = config };
    apr_table_do(check_for_cookie, &data, r->headers_in, NULL);
    if (data.token == NULL) {
        return DECLINED;
    }
    authnz_crowd_server_config *server_config = crowd_get_server_config(r->server);
    if (crowd_validate_session(r, config->crowd_config, server_config->memcached_config, data.token, &r->user) == CROWD_AUTHENTICATE_SUCCESS) {
        r->ap_auth_type = "Crowd SSO";
        crowd_set_groups_env_variable(r);
        return OK;
    }
    return DECLINED;
}

#define XLATE_BUFFER_SIZE 256

static bool xlate_string(apr_xlate_t *xlate, const char *input, char *output) {
    apr_size_t input_left = strlen(input);
    apr_size_t output_left = XLATE_BUFFER_SIZE;
    if (apr_xlate_conv_buffer(xlate, input, &input_left, output, &output_left) != APR_SUCCESS
        || input_left != 0
        || apr_xlate_conv_buffer(xlate, NULL, NULL, output, &output_left) != APR_SUCCESS
        || output_left < 1) {
        return false;
    }
    return true;
}

/* Given a username and password, expected to return AUTH_GRANTED if we can validate this user/password combination. */
static authn_status authn_crowd_check_password(request_rec *r, const char *user, const char *password)
{
    authnz_crowd_server_config *server_config = crowd_get_server_config(r->server);
    authnz_crowd_dir_config *config = get_config(r);
    if (config == NULL) {
        return AUTH_GENERAL_ERROR;
    }

    int i;
    authn_status rv = AUTH_DENIED; // by default deny access
    for (i = 0; i < config->basic_auth_encodings->nelts; i++) {
        char *encoding = APR_ARRAY_IDX(config->basic_auth_encodings, i, char *);
        apr_xlate_t *xlate;
    	if (apr_xlate_open(&xlate, "UTF-8", encoding, r->pool) != APR_SUCCESS) {
            const char *error = log_palloc(r->pool, apr_psprintf(r->pool, "Encoding not supported: '%s'", encoding));
            if (error == NULL) {
                error = "Out of memory while building error message for encoding not supported";
            }
            ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r, error);
	    break;
        }
        char xlated_user[XLATE_BUFFER_SIZE] = {};
        char xlated_password[XLATE_BUFFER_SIZE] = {};
        if (!xlate_string(xlate, user, xlated_user) || !xlate_string(xlate, password, xlated_password)) {
            ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r, "Failed to translate basic authentication credentials for user '%s', using encoding: '%s'", user, encoding);
        } else {
            crowd_authenticate_result result = CROWD_AUTHENTICATE_NOT_ATTEMPTED;
            if (config->create_sso) {
                crowd_cookie_config_t *cookie_config = crowd_get_cookie_config(r, config->crowd_config, server_config->memcached_config);
                if (cookie_config != NULL && (!cookie_config->secure || is_https(r))) {
                    const char *token;
                    result = crowd_create_session(r, config->crowd_config, server_config->memcached_config, xlated_user, xlated_password, &token);
                    if (result == CROWD_AUTHENTICATE_SUCCESS && token != NULL) {
                        char *domain = "";
                        if (cookie_config->domain != NULL && cookie_config->domain[0] == '.') {
                            size_t domainlen = strlen(cookie_config->domain);
                            size_t hostlen = strlen(r->hostname);
                            if (hostlen > domainlen
                                && strcmp(cookie_config->domain, r->hostname + hostlen - domainlen) == 0) {
                                domain = apr_psprintf(r->pool, ";Domain=%s", cookie_config->domain);
                            }
                        }
                        char *cookie = log_ralloc(r,
                            apr_psprintf(r->pool, "%s=%s%s%s;Version=1;Path=/", cookie_config->cookie_name, token,
                            domain, cookie_config->secure ? ";Secure" : ""));
                        if (cookie != NULL) {
                            apr_table_add(r->err_headers_out, "Set-Cookie", cookie);
                        }
                    }
                }
            }
            if (result == CROWD_AUTHENTICATE_NOT_ATTEMPTED) {
                result = crowd_authenticate(r, config->crowd_config, server_config->memcached_config, xlated_user, xlated_password);
            }
            switch (result) {
                case CROWD_AUTHENTICATE_SUCCESS: {
                    crowd_set_groups_env_variable(r);
                    ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r, "Authenticated '%s'.", xlated_user);
                    rv = AUTH_GRANTED;
		    break;
		}
                case CROWD_AUTHENTICATE_FAILURE:
                    break;
                default: {
                    ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r, "Crowd authentication failed due to system exception");
                    rv = AUTH_GENERAL_ERROR;
		    break;
		}
            }
        }
        apr_xlate_close(xlate);
	if (rv != AUTH_DENIED) { break; };
    }

    return rv;
}

static void *crowd_merge_config(apr_pool_t *pool, void *base_conf, 
                                   void *overrides_conf)
{
  authnz_crowd_server_config *base = (authnz_crowd_server_config *) base_conf;
  authnz_crowd_server_config *overrides = (authnz_crowd_server_config *) overrides_conf;
  authnz_crowd_server_config *config = apr_pcalloc(pool, sizeof(authnz_crowd_server_config));

  return (void *) base;
}

static const authn_provider authn_crowd_provider =
{
    &authn_crowd_check_password,    /* Callback for HTTP Basic authentication */
    NULL                            /* Callback for HTTP Digest authentication */
};

/* Called after configuration is set, to finalise it. */
static int post_config(apr_pool_t *pconf, apr_pool_t *plog __attribute__((unused)), apr_pool_t *ptemp, server_rec *s)
{

    char **error_msg = apr_pcalloc(ptemp, sizeof(char *));
    
    /* Create the caches, if required. */
    unsigned cache_max_entries
        = parse_number(authnz_crowd_process_config.cache_max_entries_string, "CrowdCacheMaxEntries", 0, UINT_MAX, 500,
        ptemp, error_msg);
    if (*error_msg != NULL) {
        ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s, *error_msg); 
        exit(1);
    }
    if (cache_max_entries > 0) {
        apr_time_t cache_max_age
            = apr_time_from_sec(parse_number(authnz_crowd_process_config.cache_max_age_string, "CrowdCacheMaxAge", 1,
            UINT_MAX, 60, ptemp, error_msg));
	if (*error_msg != NULL) {
            ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s, *error_msg); 
	    exit(1);
	}
        if (!crowd_cache_create(pconf, cache_max_age, cache_max_entries)) {
            exit(1);
        }
    }

    if (dir_configs != NULL) {

        /* Iterate over each directory config */
        authnz_crowd_dir_config **dir_config;
        while ((dir_config = apr_array_pop(dir_configs)) != NULL) {

            /* If any of the configuration parameters are specified, ensure that all mandatory parameters are
               specified. */
            crowd_config *crowd_config = (*dir_config)->crowd_config;
            if ((crowd_config->crowd_app_name != NULL || crowd_config->crowd_app_password != NULL
                || crowd_config->crowd_url != NULL)
                && (crowd_config->crowd_app_name == NULL || crowd_config->crowd_app_password == NULL
                || crowd_config->crowd_url == NULL)) {
                ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s,
                    "Missing CrowdAppName, CrowdAppPassword or CrowdURL for a directory.");
                exit(1);
            }

            if ((crowd_config->crowd_app_name != NULL || crowd_config->crowd_app_password != NULL
                || crowd_config->crowd_url != NULL)
                && (crowd_config->crowd_ssl_verify_peer == true && crowd_config->crowd_cert_path == NULL)) {
                ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s,
                        "CrowdSSLVerifyPeer is On but CrowdCertPath is unspecified, will try to use default: /etc/pki/tls/certs.");
		crowd_config->crowd_cert_path="/etc/pki/tls/certs";
            }

            /* Parse the timeout parameter, if specified */
    	    char **error_msg = apr_pcalloc(ptemp, sizeof(char *));
            crowd_config->crowd_timeout
                = parse_number((*dir_config)->crowd_timeout_string, "CrowdTimeout", 0, UINT_MAX, 0, ptemp, error_msg);
	    if (*error_msg != NULL) {
                ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s, *error_msg); 
		exit(1);
	    }

            /* If no basic auth character encodings are specified, setup ISO-8859-1. */
            if (apr_is_empty_array((*dir_config)->basic_auth_encodings)) {
                const char *error = add_basic_auth_conversion("ISO-8859-1", *dir_config, ptemp);
                if (error != NULL) {
                    ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s,
                        "Could not configure default Basic Authentication translation.  %s", error);
                    exit(1);
                }
            }
        }
	
    	authnz_crowd_server_config *server_config = crowd_get_server_config(s);
	if (server_config->memcached_config->host != NULL) {
    	    apr_status_t rv = crowd_setup_memcached(s, s->process->pool, server_config->memcached_config);
	    if (rv != APR_SUCCESS) {
	        char *error_msg = apr_palloc(plog, 100);
	        error_msg = apr_strerror(rv, error_msg, 100);
                ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s, "Failed to setup Memcached, code: %d - %s", rv, error_msg);
	    } else {
    	        rv = crowd_test_memcached_connection(s, plog, server_config->memcached_config);
	        if (rv != APR_SUCCESS) {
		    char *error_msg = apr_palloc(plog, 100);
		    error_msg = apr_strerror(rv, error_msg, 100);
                    ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s, "Failed to test connection to Memcached, code: %d - %s", rv, error_msg);
	        }
	    }
        } else {
            ap_log_error(APLOG_MARK, APLOG_INFO, 0, s, "Memcached host not configured, using in-process cache.");
	}
    }
    
    
    return OK;
}

apr_array_header_t *authnz_crowd_user_groups(const char *username, request_rec *r) {
    authnz_crowd_server_config *server_config = crowd_get_server_config(r->server);
    authnz_crowd_dir_config *config = get_config(r);
    if (config == NULL) {
        return NULL;
    }
    return crowd_user_groups(username, r, config->crowd_config, server_config->memcached_config);
}

/**
 * This hook is used to check to see if the resource being requested
 * is available for the authenticated user (r->user and r->ap_auth_type).
 * It runs after the access_checker and check_user_id hooks. Note that
 * it will *only* be called if Apache determines that access control has
 * been applied to this resource (through a 'Require' directive).
 *
 * @param r the current request
 * @return OK, DECLINED, or HTTP_...
 */
static int auth_checker(request_rec *r) {

    authnz_crowd_server_config *server_config = crowd_get_server_config(r->server);
    authnz_crowd_dir_config *config = get_config(r);
    if (config == NULL || server_config == NULL) {
    	ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r, "Config or Server config was null");
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    if (r->user == NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r, "Authorisation requested, but no user provided.");
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    /* Iterate over requirements */
    const apr_array_header_t *requires = ap_requires(r);
    apr_array_header_t *user_groups = NULL;
    int x;
    for (x = 0; x < requires->nelts; x++) {

        require_line require = APR_ARRAY_IDX(requires, x, require_line);

        /* Ignore this requirement if it does not apply to the HTTP method used in the request. */
        if (!(require.method_mask & (AP_METHOD_BIT << r->method_number))) {
            continue;
        }

        const char *next_word = require.requirement;

        /* Only process group requirements */
        if (strcasecmp(ap_getword_white(r->pool, &next_word), "group") == 0) {

            /* Fetch groups only if actually needed. */
            if (user_groups == NULL) {
                user_groups = crowd_user_groups(r->user, r, config->crowd_config, server_config->memcached_config);
                if (user_groups == NULL) {
                    return HTTP_INTERNAL_SERVER_ERROR;
                }
            }

            /* Iterate over the groups mentioned in the requirement. */
            while (*next_word != '\0') {
                const char *required_group = ap_getword_conf(r->pool, &next_word);
                /* Iterate over the user's groups. */
                int y;
                for (y = 0; y < user_groups->nelts; y++) {
                    const char *user_group = APR_ARRAY_IDX(user_groups, y, const char *);
                    if (strcasecmp(user_group, required_group) == 0) {
                        ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r,
                            "Granted authorisation to '%s' on the basis of membership of '%s'.", r->user, user_group);
                        return OK;
                    }
                }

            }
        }

    }

    ap_log_rerror(APLOG_MARK, APLOG_NOTICE, 0, r, "Denied authorisation to '%s', Authoritative=%d.", r->user, config->authoritative);
    return config->authoritative ? HTTP_UNAUTHORIZED : DECLINED;
}

static void register_hooks(apr_pool_t *p)
{
    static const char * const pre_auth_checker[]={ "mod_authz_user.c", NULL };
    ap_hook_post_config(post_config, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_check_user_id(check_user_id, NULL, NULL, APR_HOOK_FIRST);
    ap_register_provider(
        p,
        AUTHN_PROVIDER_GROUP,
        "crowd",
        "0",                    /* Version of callback interface, not the version of the implementation. */
        &authn_crowd_provider
    );
    ap_hook_auth_checker(auth_checker, pre_auth_checker, NULL, APR_HOOK_MIDDLE);
    ap_log_perror(APLOG_MARK, APLOG_NOTICE, 0, p, PACKAGE_STRING " installed.");
}

module AP_MODULE_DECLARE_DATA authnz_crowd_module =
{
    STANDARD20_MODULE_STUFF,
    create_dir_config,
    NULL,
    create_svr_config,
    crowd_merge_config,
    commands,
    register_hooks
};

/* Library initialisation and termination functions */
/* TODO: Another solution will likely be required for non-GCC platforms, e.g. Windows */

void init() __attribute__ ((constructor));

void init()
{
    crowd_init();
}

void term() __attribute__ ((destructor));

void term()
{
    crowd_cleanup();
}
