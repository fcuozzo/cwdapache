#include "crowd_memcache.h"
#include "util.h"

/* Apache httpd includes */
#include "httpd.h"
#include "http_log.h"
#include "apr.h"
#include "apr_pools.h"

memcached_config_t *crowd_create_memcached_config(apr_pool_t *p) {
    memcached_config_t *memcached_config = log_palloc(p, apr_palloc(p, sizeof(memcached_config_t)));
    if (memcached_config == NULL) {
        return NULL;
    }
    memcached_config->host=NULL;
    memcached_config->port=11211;
    memcached_config->entry_timeout=300;
    memcached_config->failed_attempts_window=120;
    memcached_config->max_failed_attempts=10;
    memcached_config->blacklist_timeout=600;
    memcached_config->pool_min=1;
    memcached_config->pool_smax=10;
    memcached_config->pool_max=30;
    memcached_config->pool_ttl=300;

    memcached_config->memcache = apr_pcalloc(p, sizeof(apr_memcache_t));
    if (memcached_config->memcache == NULL) {
	return NULL;
    }
    memcached_config->server = apr_pcalloc(p, sizeof(apr_memcache_server_t));
 
    if (memcached_config->server == NULL) {
	return NULL;
    }

    return memcached_config;
}

apr_status_t crowd_setup_memcached(const server_rec *s, apr_pool_t *p, memcached_config_t *memcached_config) {

    if (memcached_config == NULL) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "Cannot setup with a NULL configuration");
	return APR_EGENERAL;
    }

    if (memcached_config->host == NULL || strlen(memcached_config->host) == 0) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "A valid Memcached host must be specified using the CrowdMemcachedHost property.");
	exit(1);	
    }

    if (memcached_config->pool_min <= 0 || memcached_config->pool_min > memcached_config->pool_max || memcached_config->pool_smax < memcached_config->pool_min) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "The min,smax,max values for the Memcached pool have invalid values.");
	exit(1);	
    }


    apr_status_t rv = apr_memcache_create(p, 1, 0, &(memcached_config->memcache));
    if (rv != APR_SUCCESS) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "Failed to create apr_memcache_t");
	return rv;
    }
    rv = apr_memcache_server_create(p, 
	memcached_config->host, 
	memcached_config->port, 
	memcached_config->pool_min, 
	memcached_config->pool_smax, 
	memcached_config->pool_smax, 
	memcached_config->pool_ttl, 
	&(memcached_config->server));
    if (rv != APR_SUCCESS) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "Failed to create apr_memcache_server_t");
	return rv;
    }

    rv = apr_memcache_add_server(memcached_config->memcache, memcached_config->server);
    if (rv != APR_SUCCESS) {
	return rv;
    }
    return APR_SUCCESS;
}

apr_status_t crowd_test_memcached_connection(const server_rec *s, apr_pool_t *p, memcached_config_t *memcached_config) {
    if (memcached_config->memcache != NULL) {
	apr_allocator_t *allocator;
	apr_allocator_create(&allocator);
	apr_allocator_max_free_set(allocator, 1);
	apr_pool_t *temp_pool;
	apr_pool_create_ex(&temp_pool, NULL, NULL, allocator);
	apr_memcache_server_t *server = apr_palloc(temp_pool, sizeof(apr_memcache_server_t));
	apr_status_t rv = apr_memcache_server_create(temp_pool, 
					memcached_config->host, 
					memcached_config->port, 
					memcached_config->pool_min, 
					memcached_config->pool_smax, 
					memcached_config->pool_smax, 
					memcached_config->pool_ttl, 
					&(server));

	if (rv != APR_SUCCESS) {
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "Failed to create apr_memcache_server_t");
	    return rv;
	}
        char *memcache_server_version = NULL;
        rv = apr_memcache_version(server, temp_pool, &memcache_server_version);
	// I couldn't trace why the allocator/pool will SEGV when stopping httpd when this is uncommented.
	//apr_pool_clear(temp_pool);
	//apr_allocator_destroy(allocator);
	//apr_pool_destroy(temp_pool);
        if (rv == APR_SUCCESS) {
            ap_log_error(APLOG_MARK, APLOG_INFO, 0, s, "Connected with memcached server at '%s:%u' - version: %s (entry-timeout=%us, max-failed-attempts=%u, failed-attempts-window=%us, blacklist-timeout=%us)", memcached_config->host, memcached_config->port, memcache_server_version, memcached_config->entry_timeout, memcached_config->max_failed_attempts, memcached_config->failed_attempts_window, memcached_config->blacklist_timeout);
        } else {
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "Failed to test connection with Memcached process on '%s:%u'", memcached_config->host, memcached_config->port);
        }
	return APR_SUCCESS;
    }
    return APR_EGENERAL;
}
