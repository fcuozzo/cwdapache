#!/bin/bash

pkill -QUIT -n memcached;
memcached -d -m 64 -l 127.0.0.1 -p 11211 $1 
echo Memcached daemon running with PID=`pgrep -n memcached`
